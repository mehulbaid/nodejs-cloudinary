const cloudinary = require('cloudinary').v2;
const path = require('path');

function cloud() {
    cloudinary.config({
        cloud_name: process.env.cloudName,
        api_key: process.env.apiKey,
        api_secret: process.env.apiSecret
    });
}

cloud();
cloudinary.uploader.upload('avatar.png', function (err, res) {
    if (err) throw err;
    if (res) {
        console.log(res);
    }
})
// console.log(path.dirname(''));
